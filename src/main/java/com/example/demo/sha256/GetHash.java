package com.example.demo.sha256;

import java.nio.charset.StandardCharsets;

import com.google.common.hash.Hashing;

public class GetHash {
	
	public static String getSha256(String s) {
		
		String hashed = Hashing.sha256()
		        .hashString(s, StandardCharsets.UTF_8)
		        .toString();
		
		return hashed;
		
	}

}
