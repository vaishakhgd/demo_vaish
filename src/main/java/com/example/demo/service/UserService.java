package com.example.demo.service;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.transaction.Transactional;

@Service
@Transactional
public class UserService {
	
    @Autowired
    private UserRepository userRepository;
    
   
    public void saveUser(User user) {
        userRepository.save(user);
    }

  
   
    
    public User getRecentMostUser() {
       User user =  userRepository.findRecentMostPendingJob();
       
       return user;
    }
    
    
}
