package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;

@Entity
@Table(name = "users_bro_login")
public class User {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;
	
	public User() {}
	
	
	
    public String first_name;
    
    public String password;
    
    @JsonProperty(value = "first_name")
    public String getFirst_name() {
		return first_name;
    	
    }
    
    @JsonProperty(value = "first_name")
    public void setFirst_name(String first_name) {
		this.first_name = first_name;
    	
    }
    
    @JsonProperty(value = "password")
    public String getPassword() {
		return password;
    	
    }
    
    @JsonProperty(value = "password")
    public void setPassword(String password) {
		this.password = password;
    }
    	
    
    public User( String firstName, String password) {
       
        this.first_name = firstName;
        this.password = password;
    }
    
    
//other setters and getters
    
    @Override
    public String toString() {
    	return first_name +"," +password;
    }

	
}