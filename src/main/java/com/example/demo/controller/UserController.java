package com.example.demo.controller;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    UserService userService;
    
    @Autowired
    private UserRepository userRepository;

    
    @PostMapping("/wonderful")
    public String add(@RequestBody User user) {
    	System.out.println(user);
       // userService.saveUser(user);
    	User user2= userRepository.save(user);
    	
    	return user2.toString();
        
    }
    
    
    @GetMapping("/getRecentMostUser")
    public User add() {
    	
       // userService.saveUser(user);
    	User user2= userService.getRecentMostUser();
    	
    	return user2;
        
    }
    
   
}
