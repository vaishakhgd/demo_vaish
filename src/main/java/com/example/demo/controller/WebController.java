package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.User;

@Controller
public class WebController {
	
	//avoid login word
	 @RequestMapping(value = { "/goin" })
	    public String savePerson(@ModelAttribute User user, Model model) {
	 
		 model.addAttribute("goin", user);
	    
	    System.out.println(user);
	    
	    return "login";
	    
	}
	 
	
}