package com.example.demo.repository;

import com.example.demo.model.User;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends JpaRepository<User, Integer>{
	
	@Query(value = "select * from users_bro_login order by id DESC LIMIT 1",nativeQuery = true)
	User findRecentMostPendingJob(); 
}
